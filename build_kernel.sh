#! /bin/bash

[[ -z "$1" ]] && { echo Kernel version argument missing!; eselect kernel list; exit 1; }

arch=$(uname -m)
curr_version=$(uname -r | sed s/-$arch//g)
new_version=$(echo "$1" | sed s/linux-//g)
new_plain_version=$(echo "$new_version" | sed s/-gentoo//g)

jobs=$(echo "scale=0; (24 * 2/3 + 1)" | bc);
lavg=$(nproc --ignore 2);

sudo eselect kernel set $1 || exit 1
cd /usr/src/linux

if [[ "$new_version" != "$curr_version" ]];
then
	sudo cp -v /usr/src/linux-$curr_version/.config /usr/src/linux/.config
	sudo make syncconfig || exit 1
fi;

sudo make -j$jobs -l$lavg all || exit 1
sudo make -j$jobs -l$lavg modules_install || exit 1

sudo mount /boot
sudo sbsign --key "/etc/efikeys/db.key" --cert "/etc/efikeys/db.crt" --output "/boot/bzImage_$new_version-$arch.efi" "./arch/$arch/boot/bzImage"
[[ "$new_version" != "$curr_version" ]] && sudo efibootmgr -c -d /dev/nvme0n1 -p 1 -L "Gentoo Linux $new_plain_version" -l "bzImage_$new_version-$arch.efi"
sudo umount /boot

sudo EMERGE_DEFAULT_OPTS="--jobs=$jobs --load-average=$lavg" emerge @module-rebuild
for a in `emerge -qp @module-rebuild | grep "]" | awk '{ print $4 }'`;
do
	for mod in `equery f "$a" | grep "/lib/modules/" | grep ".ko"`
	do
		echo "  SIGN    $mod";
		sudo /usr/src/linux/scripts/sign-file sha256 "/etc/efikeys/db.key" "/etc/efikeys/db.pem" "$mod";
	done;
done;
sudo efibootmgr --remove-dups

echo "Removing old kernel versions...";
KEEP_VERS=2
sudo mount /boot
cd /boot
sudo find . -name '*.efi' | sort -V | head -n -$KEEP_VERS | xargs rm -v
cd
sudo umount /boot

cd /usr/src
sudo find . -maxdepth 1 -name 'linux-*-gentoo' -type d | sort -V | head -n -$KEEP_VERS | xargs rm -rfv

cd /lib/modules
sudo find . -maxdepth 1 -name '*-gentoo' -type d | sort -V | head -n -$KEEP_VERS | xargs rm -rfv

cd
for v in `efibootmgr | grep "Gentoo Linux" | sort -k4 -V | head -n -$KEEP_VERS | cut -f 1 -d " " | sed -r 's/Boot0*([1-9,A-F]+0*)\*/\1/g'`;
do
	sudo efibootmgr -v -B -b "$v";
done;
