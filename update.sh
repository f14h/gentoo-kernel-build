#!/bin/bash

jobs=$(echo "scale=0; (`nproc --all` * 2/3 + 1)" | bc);
lavg=$(nproc --ignore 2);

sudo EMERGE_DEFAULT_OPTS="--jobs=$jobs --load-average=$lavg" emerge --sync
sudo layman -S
sudo EMERGE_DEFAULT_OPTS="--jobs=$jobs --load-average=$lavg --verbose" emerge --update -1 portage
sudo emerge --deep --update --newuse --changed-use --tree --keep-going @world
sudo EMERGE_DEFAULT_OPTS="--jobs=$jobs --load-average=$lavg --verbose" emerge @preserved-rebuild
sudo revdep-rebuild -v --
sudo eclean-dist -d
sudo eix-test-obsolete
sudo EMERGE_DEFAULT_OPTS="--jobs=$jobs --load-average=$lavg --ask" emerge --depclean
echo "execute dispatch-conf if necessary..."

sudo glsa-check -t all
echo "execute glsa-check -f all to fix..."
